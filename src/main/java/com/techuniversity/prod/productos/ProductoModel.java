package com.techuniversity.prod.productos;






import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="productos")
public class ProductoModel {

    @Id
    private String id;
    private String nombre;
    private String description;
    private Double precio;

    public ProductoModel() {
    }

    public ProductoModel(String id, String nombre, String description, Double precio) {
        this.id = id;
        this.nombre = nombre;
        this.description = description;
        this.precio = precio;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }
}
