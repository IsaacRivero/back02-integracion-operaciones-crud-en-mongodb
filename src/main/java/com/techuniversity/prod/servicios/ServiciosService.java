package com.techuniversity.prod.servicios;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.*;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class ServiciosService {
    MongoCollection<Document> servicios;

    private static MongoCollection<Document> getServiciosCollection() {
        //ruta de conexión de la base de datos
        ConnectionString cs = new ConnectionString("mongodb://localhost:27017");
        //Conexion de cleinte
        MongoClientSettings settings = MongoClientSettings.builder()
                .applyConnectionString(cs)
                .retryWrites(true)
                .build();
        //creamos el cliente
        MongoClient cliente = MongoClients.create(settings);
        MongoDatabase database = cliente.getDatabase("backMayo");
        return database.getCollection("servicios");
    }

    public static List getAll() {

        //pedir una instanacia de la colleccion
        MongoCollection<Document> servicios = getServiciosCollection();
        List list = new ArrayList();
        //devuelve un tipo de collección
        FindIterable<Document> iterDoc = servicios.find();

        //levantamos instancia.
        Iterator iterador = iterDoc.iterator();

        //entramos en bucle mientras tengamos mas, y devolvemos todos.
        while (iterador.hasNext()) {
            list.add(iterador.next());
        }
        //devolvemos lista
        return list;
    }

    public static void insert(String cadenaServicio) throws Exception {
        Document doc = Document.parse(cadenaServicio);
        // puntero hacia la colección
        MongoCollection<Document> servicios = getServiciosCollection();
        servicios.insertOne(doc);
    }

    public static void insertBatch(String cadenaServicios) throws Exception {
        Document doc = Document.parse(cadenaServicios);
        List<Document> lstServicios = doc.getList("servicios", Document.class);
        // puntero hacia la colección
        MongoCollection<Document> servicios = getServiciosCollection();
        servicios.insertOne(doc);
    }

    public static List<Document> getFiltrados(String cadenaFiltro) {
        MongoCollection<Document> servicios = getServiciosCollection();
        List lista = new ArrayList();
        Document docFiltro =  Document.parse(cadenaFiltro);
        //devuelve un tipo de collección
        FindIterable<Document> iterDoc = servicios.find(docFiltro);
        //levantamos instancia.
        Iterator iterador = iterDoc.iterator();
        //entramos en bucle mientras tengamos mas, y devolvemos todos.
        while (iterador.hasNext()) {
            lista.add(iterador.next());
        }
        //devolvemos lista
        return lista;
    }

    public static List<Document> getFiltradosPeriodo(Document docFiltro) {
        MongoCollection<Document> servicios = getServiciosCollection();
        List lista = new ArrayList();
        //devuelve un tipo de collección
        FindIterable<Document> iterDoc = servicios.find(docFiltro);
        //levantamos instancia.
        Iterator iterador = iterDoc.iterator();
        //entramos en bucle mientras tengamos mas, y devolvemos todos.
        while (iterador.hasNext()) {
            lista.add(iterador.next());
        }
        //devolvemos lista
        return lista;
    }

    public  static void update(String filtro, String valores) {
        MongoCollection<Document> servicios = getServiciosCollection();
        //Combierto en documentos las cadenas sring
        Document docFiltro = Document.parse(filtro);
        Document docValores = Document.parse(valores);
        //solo actualizamos uno, con el documento de filtro y los valores que vamos a modificar.
        servicios.updateOne(docFiltro,docValores);
    }
}
