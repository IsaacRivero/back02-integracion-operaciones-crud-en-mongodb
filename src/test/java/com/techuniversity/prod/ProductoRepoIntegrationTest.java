package com.techuniversity.prod;

import com.techuniversity.prod.controllers.ProductoController;
import com.techuniversity.prod.productos.ProductoModel;
import com.techuniversity.prod.productos.ProductoRepository;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class ProductoRepoIntegrationTest {

    @Autowired
    ProductoRepository productoRepository;

    @Test
    public void testFindAll() {
        List<ProductoModel> productoModels = productoRepository.findAll();
        assertTrue(productoModels.size() > 0);
    }

    @LocalServerPort
    private int port;
    TestRestTemplate testRestTemplate = new TestRestTemplate();
    HttpHeaders httpHeaders = new HttpHeaders();

    @Test
    public void testPrimerProducto() throws Exception {
        HttpEntity<String> httpEntity = new HttpEntity<String>(null,httpHeaders);
        ResponseEntity<String> responseEntity = testRestTemplate.exchange(
                crearUrlport("/productos/productos/60adf8ab25e729160c50883e"),
                HttpMethod.GET,
                httpEntity, String.class);
        String expected = "{\"id\":\"60adf8ab25e729160c50883e\"," + "\"nombre\":\"Isaac\",\"descripcion\":\"producto 1\"," + "\"precio\":21.5}";
        System.out.println(responseEntity.getBody());
        System.out.println(expected);
        JSONAssert.assertNotEquals(expected, responseEntity.getBody(), false);

    }

    private String crearUrlport(String url) {
        return "http://localhost:"+ port + url;
    }

}
